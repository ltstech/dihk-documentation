# DIHK API SERVER
-------------------------------------

## [ Get Start ] 
-------------------------------------
###### Install the package
```
$ npm install yarn -g
$ yarn
```
###### setup ENV config (copy and fill it up)
```
$ cp run.env.sample run.env
```
###### Run the Project
```
$ ./dev.sh
```

## [ Cron Job ]
-------------------------------------
Cron Job should be run once a day at the same time everyday between HKT 8:30 to 23:30. Recommended time is HKT 9:00.

###### List of tasks
```
- event-finished: For event in "CONFIRMED", "APPROVED" status. If all bookings of the event are finished, update the event status to "FINISHED", "DIHK-EVENT-FINISHED" email should be send out.
- event-remind-onhold-16: For event in "ONHOLD" status. If the first booking of the event are start in 16 days, "DIHK-EVENT-REMIND-ONHOLD-16" email should be send out.
- event-onhold-release: For event in "ONHOLD" status. If the first booking of the event are start in 14 days, update the event status to "DRAFT".
- event-remind-7: For event in "CONFIRMED", "APPROVED" status. If the first booking of the event are start in 7 days, "DIHK-EVENT-REMIND-7" email should be send out.
```
###### Install the package
```
$ npm install yarn -g
$ yarn
```
###### setup ENV config (copy and fill it up)
```
$ cp run.env.sample cron.env
```
###### Run the Project
```
$ ./cron.sh
```

## [ DevOps Reference Files ]
-------------------------------------
###### Config
```
./run.env.sample
./config/custom-environment-variables.json
./config/default.json
```
###### Build docker image
```
./docker-build.sh
./Dockerfile
```
###### Run docker image
```
./docker-compose.yml
./dev.sh
```
###### Cron job
```
./cron.sh
```

## [ Email Triggers ]
-------------------------------------
```
- You "must" use the same Template Name listed below in Sendgrid.
- Parameters that passing into the email. in the email template, you have to use {{ }} for it. eg. {{requesterName}}
- To, Cc, and Trigger is just FYI
```
|Template Name|To|Cc|Parameters|Triggers|
|----|----|----|----|----|
|DIHK-OWNER-INVITE-RP|Invited user|N/A|token, invitedEmail, requesterName, partnerName|Invite 1st owner to RESIDENT partner|
|DIHK-OWNER-INVITE-NRP|Invited user|N/A|token, invitedEmail, requesterName, partnerName|Invite 1st owner to NONRESIDENT partner|
|DIHK-OWNER-INVITE-PUBLIC|Invited user|N/A|token, invitedEmail, requesterName, partnerName|Invite 1st owner to PUBLIC partner|
|DIHK-USER-INVITE|Invited user|N/A|token, invitedEmail, requesterName, partnerName|Invite to partner (non 1st owner)|
|DIHK-USER-WELCOME|Signup user|N/A|userName, partnerName|New registration, Invite registration |
|DIHK-EVENT-ONHOLD|Event Creator|receive reminder users|eventName, bookingsTable, eventStartDate, partnerName|On hold event|
|DIHK-EVENT-CONFIRMED|Event Creator|receive reminder users|eventName, bookingsTable, eventStartDate, partnerName|Confirm event|
|DIHK-EVENT-APPROVED|Event Creator|receive reminder users|eventName, bookingsTable, eventStartDate, partnerName|Approval event|
|DIHK-EVENT-CANCELED-ONHOLD|Event Creator|receive reminder users|eventName, bookingsTable, eventStartDate, partnerName|Cancel ONHOLD event|
|DIHK-EVENT-CANCELED|Event Creator|receive reminder users|eventName, bookingsTable, eventStartDate, partnerName|Cancel event before 14 days|
|DIHK-EVENT-CANCELED-IN14DAYS|Event Creator|receive reminder users|eventName, bookingsTable, eventStartDate, partnerName|Cancel event within 14 days|
|DIHK-EVENT-CANCELED-IN24HOURS|Event Creator|receive reminder users|eventName, bookingsTable, eventStartDate, partnerName|Cancel event within 24 hours|
|DIHK-EVENT-FINISHED|Event Creator|receive reminder users|eventName, bookingsTable, eventStartDate, partnerName|Finish event by cron job|
|DIHK-EVENT-FEEDBACK|Event Creator|receive reminder users|eventName, bookingsTable, eventStartDate, partnerName|Only for PUBLIC partner, Finish event by cron job|
|DIHK-EVENT-RELEASE|Event Creator|receive reminder users|eventName, bookingsTable, eventStartDate, partnerName|Release event by cron job|
|DIHK-EVENT-REMIND-ONHOLD-16|Event Creator|receive reminder users|eventName, bookingsTable, eventStartDate, partnerName|16 days before on hold event by cron job|
|DIHK-EVENT-REMIND-7|Event Creator|receive invoice users|eventName, bookingsTable, eventStartDate, partnerName|7 days before event by cron job|
|DIHK-EVENT-INVOICE|Event Creator|receive invoice users|eventName, bookingsTable, eventStartDate, partnerName|Confirm event|
|DIHK-EVENT-INVOICE-PAID|Event Creator|receive invoice users|eventName, bookingsTable, eventStartDate, partnerName|Invoice paid|
|DIHK-EVENT-INVOICE-CANCELED|Event Creator|receive invoice users|eventName, bookingsTable, eventStartDate, partnerName|Cancel event|

## [ Event Booking ]
-------------------------------------
#### Event Booking Flow
###### *Admin can change event status without following the rules below.*
||DRAFT|ONHOLD|CONFIRMED|APPROVED|FINISHED|CANCELED|
|----|:----:|:----:|:----:|:----:|:----:|:----:|
|On Hold Event|✓ *14 days before event*|x|x|x|x|x|
|Confirm Event|✓|✓|x|x|x|x|
|Approve Event|✓|✓|✓|x|x|x|
|Cancel Event|✓|✓|✓|✓|x|x|
|Delete Event|✓|x|x|x|x|x|
|Update Event Detail|✓|✓|✓ *24 Hours before event*|✓ *24 Hours before event*|x|x|
|Update Booking|✓|✓|x|x|x|x|
|Update Room Setup|✓|✓|✓ *24 Hours before event*|✓ *24 Hours before event*|x|x|

## [ Google Calendar ]
-------------------------------------
###### To check the Google connection status
```
- Connect: {{apiUrl}}/google/status
```
###### To connect the Google account
```
You will found the following link from the response header when you login as SYSTEMADMIN.
- Connect: {{apiUrl}}/google/authorize?authorization=Bearer%20{{accessToken}}
```
###### Calendar Event
|DRAFT|ONHOLD|CONFIRMED|APPROVED|FINISHED|CANCELED|DELETED
|:----:|:----:|:----:|:----:|:----:|:----:|:----:|
|x|✓|✓|✓|✓|x|x|

## [ Quickbooks ]
-------------------------------------
IMPORTANT: Please turn off `Automatically apply credits` from Quickbooks account setting.
###### To check the Quickbooks connection status
```
- Connect: {{apiUrl}}/quickbooks/status
```
###### To connect the Quickbooks account
```
You will found the following link from the response header when you login as SYSTEMADMIN.
- Connect: {{apiUrl}}/quickbooks/authorize?authorization=Bearer%20{{accessToken}}
```

###### Admin changed event status by PUT /event/:id/official-setting for DRAFT, ONHOLD, FINISHED, CANCELED will not change the invoice.
|Action|Invoice|
|----|----|
|Approve Event|Invoice PENDING|
|Cancel Event over 14 days|Invoice VOIDED, No Cancellation Fee|
|Cancel Event within 14 days|Invoice Status Unchanged, 50% Cancellation Fee| 
|Cancel Event within 24 hours|Invoice Status Unchanged, 100% Cancellation Fee| 
|Create Payment| Invoice PAID, update payment reference|

## [ Other Documentations ]
-------------------------------------
```
You will found the following link from the response header when you login as SYSTEMADMIN.
- Swagger: {{apiUrl}}/swagger
- Graphiql: {{apiUrl}}/graphiql?authorization=Bearer%20{{accessToken}}
```

## [ Config ]
-------------------------------------
Server default UTC+0 timezone, default booking hours from HKT 9:30am to 9:30pm.

###### Default Config
```
{
    "debug": false, // Turn on debug log
    "log":
    {
        "path": "", // Location path for log file
        "level": "info" // log level
    },
    "mongo":
    {
        "url": "mongodb://localhost:27017/dihk", // Mongodb connection url
        "secret": "D!HKM0NG0$ECRET" // Mongodb secret for encrypt sensitive information in db
    },
    "booking":
    {
        "night":
        {
            "hour": "10", // Booking Night time hour start, UTC 10:00 === HKT 18:00
            "min": "00"
        }
    },
    "tokens":
    {
        "access":
        {
            "expiry": 720, // Token expiry in Minutes, 720 = 12 hrs
            "secret": "D!HK@CCE$$$ECRET" // Secret for encrypt JWT Token
        },
        "refresh":
        {
            "expiry": 2880, // Token expiry in Minutes, 2880 = 48 hrs
            "secret": "D!HKREFRE$H$ECRET" // Secret for encrypt JWT Token
        },
        "invite":
        {
            "secret": "D!HK!NV!T@T!0N$ECRET" // Secret for encrypt JWT Token
        }
    },
    "calendar":
    {
        "holiday": "en.hong_kong#holiday@group.v.calendar.google.com" // Calendar id for holiday checking
    },
    "quickbooks":
    {
        "id": "ABlFXhxm5X2JQGbWVuH06ZlUhyXVi6DGzfiUn9uj3rFb9QXrBU", // Quickbooks develop id
        "secret": "RyWKfuksFPwxfjTMNQRr1oiSIavssdQ4UF1MhwYQ", // Quickbooks develop secret
        "env": "production", // Quickbooks switch between production and sandbox
        "debug": false, // Quickbooks library debug log
        "items":
        {
            "booking": // Quickbooks product names for booking invoice
            {
                "resident": "RP venue fee", 
                "nonresident": "NRP Venue Fee",
                "public": "Outsider venue fee"
            }
        }
    },
    "sendgrid":
    {
        "secret": "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", 
        "from":
        {
            "email": "no-reply-contact-request@dreamimpacthk.com", // Email sender
            "name": "Dream Impact Team" // Email sender name
        },
        "footer": // Email footer information
        {
            "name": "Copyright © 2019 Dream Impact, All rights reserved.",
            "address": "Address: Unit C · 4/F · 760 Cheung Sha Wan Road · Lai Chi Kok · Kowloon · Hong Kong",
            "unsubscribe": "https://dreamimpact.deepwork.ai/#unsubscribe",
            "preferences": "https://dreamimpact.deepwork.ai/#unsubscribe-preferences"
        }
    },
    "server":
    {
        "url": "https://api-dream.deepwork.ai", // Domain of this server running on, use for create links
        "origins": "dreamimpact.deepwork.ai,*", // CORS allow access origins
        "headers": "Authorization,Accept,Accept-Version,Content-Length,Content-MD5,Content-Type,Content-Disposition,Date,Cache-Control,Origin,X-Api-Version,api-version,content-length,content-md5,content-type,date,request-id,response-time,accept,accept-version,origin,x-api-version,x-request-id,x-requested-with,Swagger-Site,Graphiql-Site,Google-Connect-Site,Quickbooks-Connect-Site", // CORS allow headers origins
        "name": "DIHK-API", // Name of this server, will show in the log
        "port": 8310 // Port starting this server on
    }
}
```
##### Env Config Name
```
{
    "debug": "API_DEBUG",
    "log":
    {
        "path": "API_LOG_PATH",
        "level": "API_LOG_LEVEL"
    },
    "mongo":
    {
        "url": "API_MONGO_URL",
        "secret": "API_MONGO_SECRET"
    },
    "booking":
    {
        "night":
        {
            "hour": "API_BOOKING_NIGHT_HOUR",
            "min": "API_BOOKING_NIGHT_MIN"
        }
    },
    "tokens":
    {
        "access":
        {
            "expiry": "API_TOKENS_ACCESS_EXPIRY",
            "secret": "API_TOKENS_ACCESS_SECRET"
        },
        "refresh":
        {
            "expiry": "API_TOKENS_REFRESH_EXPIRY",
            "secret": "API_TOKENS_REFRESH_SECRET"
        },
        "invite":
        {
            "secret": "API_TOKENS_INVITE_SECRET"
        }
    },
    "calendar":
    {
        "holiday": "API_CALENDAR_HOLIDAY"
    },
    "quickbooks":
    {
        "id": "API_QUICKBOOKS_ID",
        "secret": "API_QUICKBOOKS_SECRET",
        "env": "API_QUICKBOOKS_ENV",
        "debug": "API_QUICKBOOKS_DEBUG",
        "items":
        {
            "booking":
            {
                "resident": "API_QUICKBOOKS_ITEMS_BOOKING_RESIDENT",
                "nonresident": "API_QUICKBOOKS_ITEMS_BOOKING_NONRESIDENT",
                "public": "API_QUICKBOOKS_ITEMS_BOOKING_PUBLIC"
            }
        }
    },
    "sendgrid":
    {
        "secret": "API_SENDGRID_SECRET",
        "from":
        {
            "email": "API_SENDGRID_FROM_EMAIL",
            "name": "API_SENDGRID_FROM_NAME"
        },
        "footer":
        {
            "name": "API_SENDGRID_FOOTER_NAME",
            "address": "API_SENDGRID_FOOTER_ADDRESS",
            "unsubscribe": "API_SENDGRID_FOOTER_UNSUBSCRIBE",
            "preferences": "API_SENDGRID_FOOTER_PREFERENCES"
        }
    },
    "server":
    {
        "url": "API_SERVER_URL",
        "origins": "API_SERVER_ORIGINS",
        "headers": "API_SERVER_HEADERS",
        "name": "API_SERVER_NAME",
        "port": "API_SERVER_PORT"
    }
}
```

## [ Test Result ]
-------------------------------------
###### Unit Testing
While the testing world can be filled with misnomers, the easiest way to think about a "unit test" and APIs is testing a single endpoint, with a single request, looking for a single response or set of responses. Many times, this type of testing can be done manually via the command line and something like a cURL command or with lightweight tools like SoapUI.
###### Integration Testing
Integration testing is the most often used form of API testing, as APIs are at the center of most integrations between internal or third-party services 
###### End-to-End Testing
End-to-End testing can help us validate the flow of data and information between a few different API connections. 
###### Performance Testing
We are trying to change the paradigm of Load testing and shift it left into every commit. Previously, load testing was kept in the hands of the few and was difficult to execute in a CI/CD environment. LoadUI Pro is a performance testing tool for RESTful, SOAP, and other web services that enables nearly any team member to embed performance tests into their CI/CD pipeline.
###### Install the package
```
$ npm install yarn -g
$ yarn
```
###### Run the test
```
$ yarn run test
```
###### Local Test Result as at 12Jul2019
```
  Auth Endpoints
    ✓ POST /auth/login, Should login as admin (400ms)
    ✓ POST /auth/register, Should signup as user (367ms)
    ✓ POST /auth/register, Should return email already signup
    ✓ POST /auth/password, Should update user password (655ms)
    ✓ POST /auth/login, Should login as user (331ms)
    ✓ POST /auth/logout, Should logout user
    ✓ POST /auth/logout, Should return token error
    ✓ POST /auth/password/reset, Should update user password (346ms)
    ✓ POST /auth/login, Should login as user (341ms)
    ✓ POST /auth/token, Should refresh token

  User Endpoints
    ✓ Should have auth token
    ✓ GET /users, Should return permission error
    ✓ GET /user/mine, Should return user info
    ✓ GET /users?partner=partnerId, Should return users info
    ✓ GET /user/:id, Should return user populated info
    ✓ PUT /user/:id/profile, Should update user profile
    ✓ PUT /user/:id/system-access, Should update user to SYSTEMADMIN by admin
    ✓ POST /user/:id/suspend, Should suspend user by owner
    ✓ GET /user/mine, Should return user suspended error
    ✓ PUT /user/:id/system-access, Should update user system-access by admin
    ✓ PUT /user/:id/partner-setting, Should update user partner-setting by owner
    ✓ PUT /user/:id/partner-setting, Should not update user partner-setting by user
    ✓ PUT /user/:id/partner-setting, Should update user partner-setting by admin
    ✓ PUT /user/:id/official-setting, Should update user official-setting by admin

  Partner Endpoints
    ✓ Should have auth token
    ✓ GET /partners?brname=partnerBrname, Should return partners info
    ✓ PUT /partner/:id/profile, Should update partner profile
    ✓ PUT /partner/:id/payment, Should update partner payment
    ✓ GET /partner/:id/payment, Should get partner payment
    ✓ PUT /partner/:id/official-setting, Should update partner official-setting
    ✓ GET /partner/:id, Should return partner info

  Rooms Endpoints
    ✓ Should have auth token
    ✓ POST /rooms, Should create new room
    ✓ GET /rooms?sort=-createdAt&name=Testing Room To Be Delete, Should find rooms
    ✓ GET /rooms/availabilities?startDate=Fri Jul 12 2019 19:45:03 GMT+0800 (Hong Kong Standard Time)&endDate=Sat Jul 13 2019 19:45:03 GMT+0800 (Hong Kong Standard Time), Should return rooms availabilities
    ✓ PUT /room/:id, Should update room description
    ✓ POST  /room/:id/prices, Should create room price 
    ✓ POST /room/:id/showcases, Should create room showcase
    ✓ GET  /room/:id, Should find room
    ✓ DEL  /room/:id/showcase/abc123456789, Should delete room showcase
    ✓ DEL /room/:id, Should delete room
    ✓ GET  /room/:id, Should return room not found

  Event Endpoints
    ✓ Should have auth token
    ✓ POST /partner/:id/events, Should create new event
    ✓ GET /partner/:id/events, Should get partner's events
    ✓ GET /events?name=Testing Event To Be Delete, Should get events
    ✓ PUT /event/:id, Should update event
    ✓ GET /event/:id, Should get event
    ✓ GET /event/:id/check, Should check the event and return error

  Booking Endpoints
    ✓ Should have auth token
    ✓ GET /rooms/availabilities?startDate=Sat, 04 Jan 2020 02:00:03 GMT&endDate=Sat, 04 Jan 2020 13:00:03 GMT, Should return rooms availabilities
    ✓ POST /event/:id/bookings, Should create new booking (701ms)
    ✓ GET /event/:id/bookings, Should return event's bookings
    ✓ GET /bookings?page=1&size=10&partner=partnerId, Should return bookings in pagination
    ✓ PUT /booking/:id, Should update booking (688ms)
    ✓ GET /booking/:id, Should return booking
    ✓ DEL /booking/:id, Should delete booking
    ✓ GET /booking/:id, Should return booking not found

  Setup Endpoints
    ✓ Should have auth token
    ✓ POST /event/:id/setups, Should create new setup
    ✓ GET /event/:id/setups, Should return no setups when no related room booking
    ✓ GET /setups?page=1&size=10&partner=partnerId, Should return setups in pagination
    ✓ PUT /setup/:id, Should update setup
    ✓ PUT /setup/:id/layout, Should update setup
    ✓ GET /setup/:id, Should return setup
    ✓ DEL /setup/:id, Should delete setup
    ✓ GET /setup/:id, Should return setup not found

  Metadata Endpoints
    ✓ Should have auth token
    ✓ POST /metadatas, Should create new metadata
    ✓ POST /metadatas/keys, Should set metadata
    ✓ GET /metadatas?key=testKey, Should return metadatas
    ✓ PUT /metadata/testKey, Should update metadata
    ✓ GET /metadata/testKey, Should return metadata
    ✓ DEL /metadata/testKey, Should delete metadata
    ✓ GET /metadata/testKey, Should return metadata not found

  Google Endpoints
    ✓ GET /google/status, Should be online (436ms)

  Quickbooks Endpoints
    ✓ GET /quickbooks/status, Should be online (822ms)

  Event -> Booking -> Invoice Flow
    ✓ POST /auth/register, Should have an invoice user
    ✓ POST /auth/login, Should login as invoice user (345ms)
    ✓ Should have auth token
    ✓ DEL /event/:id, Should delete event
    ✓ GET /event/:id, Should get event
    ✓ GET /user/mine, Should return invoice user info
    ✓ PUT /partner/:id/official-setting, Should update partner official-setting
    ✓ POST /partner/:id/events, Should create new event
    ✓ GET /rooms/availabilities?startDate=Sat, 04 Jan 2020 02:00:03 GMT&endDate=Sat, 04 Jan 2020 13:00:03 GMT, Should return rooms availabilities
    ✓ POST /event/:id/bookings, Should create new booking (681ms)
    ✓ GET /bookings?page=1&size=10&partner=partnerId, Should return bookings in pagination
    ✓ GET /event/:id/check, Should check the event
    ✓ POST /event/:id/hold, Should on hold event
    ✓ POST /event/:id/confirm, Should confirm event (46ms)
    ✓ PUT /event/:id/official-setting, Should approve event (2377ms)
    ✓ GET /invoices?partner=partnerId, Should return the partner's invoices
    ✓ GET /invoices?event=eventId, Should return the event's invoices
    ✓ GET /invoice/:id, Should return the invoice
    ✓ GET /invoice/:id/pdf, Should return the invoice pdf (1450ms)
    ✓ POST /invoice/:id/pay, Should add payment to invoice (3569ms)
    ✓ POST /event/:id/cancel, Should cancel event (1694ms)
    ✓ GET /invoice/:id, Should return the invoice voided

  Partner -> Invite -> Signup Flow
    ✓ Should have auth token
    ✓ POST /partner/:id/invite, Should allow standard user invite user
    ✓ DEL /partner/:id/invite/:email, Should delete invitation
    ✓ POST /partner/:id/invite, Should not allow standard user invite user as admin
    ✓ POST /partner/:id/invite, Should allow standard admin invite user as admin
Delay 5 sec...
    ✓ GET /systemadmin/email/records?page=1&size=1&sort=-createdAt&templateName=DIHK-USER-INVITE&email=email, Should return email record (5021ms)
    ✓ GET /auth/invite-register/validate?token=token, Should validate token
    ✓ POST /auth/invite-register, Should signup as admin (364ms)
    ✓ POST /auth/login, Should login as admin (347ms)
    ✓ GET /user/mine, Should return user info

  System Admin Endpoints
    ✓ Should have auth token
{ room: '5d2872c102a8390006e6cca5,5d2872c102a8390006e6cca5',
  user: '5d2872bf02a8390006e6cc9c,5d2872d402a8390006e6cccd',
  partner:
   '5d2872bf02a8390006e6cc9d,5d2872bf02a8390006e6cc9d,5d2872bf02a8390006e6cc9d',
  event:
   '5d2872c102a8390006e6cca8,5d2872c102a8390006e6cca8,5d2872c502a8390006e6ccb2',
  booking:
   '5d2872c202a8390006e6cca9,5d2872c202a8390006e6cca9,5d2872c502a8390006e6ccb3,5d2872c502a8390006e6ccb3',
  setup: '5d2872c302a8390006e6ccae',
  invoice: '5d2872c602a8390006e6ccba,5d2872c602a8390006e6ccba',
  metadata: '5d2872c302a8390006e6ccaf' }
[ { n: 2, ok: 1, deletedCount: 2 },
  { n: 1, ok: 1, deletedCount: 1 },
  { n: 1, ok: 1, deletedCount: 1 },
  { n: 2, ok: 1, deletedCount: 2 },
  { n: 2, ok: 1, deletedCount: 2 },
  { n: 1, ok: 1, deletedCount: 1 },
  { n: 1, ok: 1, deletedCount: 1 },
  { n: 0, ok: 1, deletedCount: 0 } ]
    ✓ DEL /systemadmin/test/data, Should remove testing data


  111 passing (22s)

Done in 22.96s.

```
###### Live Test Result as at 06Jul2019
```
  Auth Endpoints
    ✓ POST /auth/login, Should login as admin (968ms)
    ✓ POST /auth/register, Should signup as user (621ms)
    ✓ POST /auth/register, Should return email already signup (168ms)
    ✓ POST /auth/password, Should update user password (950ms)
    ✓ POST /auth/login, Should login as user (810ms)
    ✓ POST /auth/logout, Should logout user (180ms)
    ✓ POST /auth/logout, Should return token error (170ms)
    ✓ POST /auth/password/reset, Should update user password (605ms)
    ✓ POST /auth/login, Should login as user (642ms)
    ✓ POST /auth/token, Should refresh token (182ms)

  User Endpoints
    ✓ Should have auth token
    ✓ GET /users, Should return permission error (168ms)
    ✓ GET /user/mine, Should return user info (175ms)
    ✓ GET /users?partner=partnerId, Should return users info (165ms)
    ✓ GET /user/:id, Should return user populated info (185ms)
    ✓ PUT /user/:id/profile, Should update user profile (173ms)
    ✓ PUT /user/:id/system-access, Should update suspended user by admin (175ms)
    ✓ GET /user/mine, Should return user suspended error (161ms)
    ✓ PUT /user/:id/system-access, Should update user system-access by admin (202ms)
    ✓ PUT /user/:id/partner-setting, Should update user partner-setting by owner (182ms)
    ✓ PUT /user/:id/partner-setting, Should not update user partner-setting by user (174ms)
    ✓ PUT /user/:id/partner-setting, Should update user partner-setting by admin (179ms)
    ✓ PUT /user/:id/official-setting, Should update user official-setting by admin (176ms)

  Partner Endpoints
    ✓ Should have auth token
    ✓ GET /partners?brname=partnerBrname, Should return partners info (198ms)
    ✓ PUT /partner/:id/profile, Should update partner profile (212ms)
    ✓ PUT /partner/:id/payment, Should update partner payment (180ms)
    ✓ GET /partner/:id/payment, Should get partner payment (181ms)
    ✓ PUT /partner/:id/official-setting, Should update partner official-setting (183ms)
    ✓ GET /partner/:id, Should return partner info (168ms)

  Rooms Endpoints
    ✓ Should have auth token
    ✓ POST /rooms, Should create new room (179ms)
    ✓ GET /rooms?sort=-createdAt&name=Testing Room To Be Delete, Should find rooms (173ms)
    ✓ GET /rooms/availabilities?startDate=Sat Jul 06 2019 11:42:52 GMT+0800 (Hong Kong Standard Time)&endDate=Sun Jul 07 2019 11:42:52 GMT+0800 (Hong Kong Standard Time), Should return rooms availabilities (194ms)
    ✓ PUT /room/:id, Should update room description (194ms)
    ✓ POST  /room/:id/prices, Should create room price  (186ms)
    ✓ POST /room/:id/showcases, Should create room showcase (190ms)
    ✓ GET  /room/:id, Should find room (185ms)
    ✓ DEL  /room/:id/showcase/abc123456789, Should delete room showcase (172ms)
    ✓ DEL /room/:id, Should delete room (179ms)
    ✓ GET  /room/:id, Should return room not found (180ms)

  Event Endpoints
    ✓ Should have auth token
    ✓ POST /partner/:id/events, Should create new event (193ms)
    ✓ GET /partner/:id/events, Should get partner's events (174ms)
    ✓ GET /events?name=Testing Event To Be Delete, Should get events (204ms)
    ✓ PUT /event/:id, Should update event (175ms)
    ✓ GET /event/:id, Should get event (189ms)
    ✓ GET /event/:id/check, Should check the event and return error (189ms)

  Booking Endpoints
    ✓ Should have auth token
    ✓ GET /rooms/availabilities?startDate=Sun, 29 Dec 2019 02:00:52 GMT&endDate=Sun, 29 Dec 2019 13:00:52 GMT, Should return rooms availabilities (201ms)
    ✓ POST /event/:id/bookings, Should create new booking (220ms)
    ✓ GET /event/:id/bookings, Should return event's bookings (187ms)
    ✓ GET /bookings?page=1&size=10&partner=partnerId, Should return bookings in pagination (188ms)
    ✓ PUT /booking/:id, Should update booking (196ms)
    ✓ GET /booking/:id, Should return booking (178ms)
    ✓ DEL /booking/:id, Should delete booking (186ms)
    ✓ GET /booking/:id, Should return booking not found (173ms)

  Setup Endpoints
    ✓ Should have auth token
    ✓ POST /event/:id/setups, Should create new setup (192ms)
    ✓ GET /event/:id/setups, Should return no setups when no related room booking (206ms)
    ✓ GET /setups?page=1&size=10&partner=partnerId, Should return setups in pagination (184ms)
    ✓ PUT /setup/:id, Should update setup (182ms)
    ✓ PUT /setup/:id/layout, Should update setup (184ms)
    ✓ GET /setup/:id, Should return setup (197ms)
    ✓ DEL /setup/:id, Should delete setup (174ms)
    ✓ GET /setup/:id, Should return setup not found (174ms)

  Metadata Endpoints
    ✓ Should have auth token
    ✓ POST /metadatas, Should create new metadata (167ms)
    ✓ POST /metadatas/keys, Should set metadata (192ms)
    ✓ GET /metadatas?key=testKey, Should return metadatas (179ms)
    ✓ PUT /metadata/testKey, Should update metadata (186ms)
    ✓ GET /metadata/testKey, Should return metadata (173ms)
    ✓ DEL /metadata/testKey, Should delete metadata (182ms)
    ✓ GET /metadata/testKey, Should return metadata not found (185ms)

  Google Endpoints
    ✓ GET /google/status, Should be online (247ms)

  Quickbooks Endpoints
    ✓ GET /quickbooks/status, Should be online (1177ms)

  Event -> Booking -> Invoice Flow
    ✓ POST /auth/register, Should have an invoice user (178ms)
    ✓ POST /auth/login, Should login as invoice user (568ms)
    ✓ Should have auth token
    ✓ DEL /event/:id, Should delete event (206ms)
    ✓ GET /event/:id, Should get event (180ms)
    ✓ GET /user/mine, Should return invoice user info (189ms)
    ✓ PUT /partner/:id/official-setting, Should update partner official-setting (184ms)
    ✓ POST /partner/:id/events, Should create new event (203ms)
    ✓ GET /rooms/availabilities?startDate=Sun, 29 Dec 2019 02:00:52 GMT&endDate=Sun, 29 Dec 2019 13:00:52 GMT, Should return rooms availabilities (184ms)
    ✓ POST /event/:id/bookings, Should create new booking (930ms)
    ✓ GET /bookings?page=1&size=10&partner=partnerId, Should return bookings in pagination (186ms)
    ✓ GET /event/:id/check, Should check the event (171ms)
    ✓ POST /event/:id/hold, Should on hold event (209ms)
    ✓ POST /event/:id/confirm, Should confirm event (2041ms)
    ✓ GET /invoices?partner=partnerId, Should return the partner's invoices (179ms)
    ✓ GET /invoices?event=eventId, Should return the event's invoices (175ms)
    ✓ GET /invoice/:id, Should return the invoice (193ms)
    ✓ GET /invoice/:id/pdf, Should return the invoice pdf (1421ms)
    ✓ PUT /event/:id/official-setting, Should approve event (1996ms)
    ✓ POST /invoice/:id/pay, Should add payment to invoice (3084ms)
    ✓ POST /event/:id/cancel, Should cancel event (1598ms)
    ✓ GET /invoice/:id, Should return the invoice voided (176ms)

  Partner -> Invite -> Signup Flow
    ✓ Should have auth token
    ✓ POST /partner/:id/invite, Should allow standard user invite user (183ms)
    ✓ DEL /partner/:id/invite/:email, Should delete invitation (195ms)
    ✓ POST /partner/:id/invite, Should not allow standard user invite user as admin (180ms)
    ✓ POST /partner/:id/invite, Should allow standard admin invite user as admin (215ms)
Delay 5 sec...
    ✓ get /systemadmin/email/records?page=1&size=1&sort=-createdAt&templateName=DIHK-USER-INVITE&email=email, Should return email record (5251ms)
    ✓ get /auth/invite-register/validate?token=token, Should validate token (171ms)
    ✓ POST /auth/invite-register, Should signup as admin (661ms)
    ✓ POST /auth/login, Should login as admin (639ms)
    ✓ GET /user/mine, Should return user info (175ms)

  System Admin Endpoints
    ✓ Should have auth token
{ room: '5d2018c4e2fb6b0006aed4f2,5d2018c4e2fb6b0006aed4f2',
  user: '5d2018bde2fb6b0006aed4e9,5d2018e1e2fb6b0006aed536',
  partner:
   '5d2018bde2fb6b0006aed4ea,5d2018bde2fb6b0006aed4ea,5d2018bde2fb6b0006aed4ea',
  event:
   '5d2018c6e2fb6b0006aed4f5,5d2018c6e2fb6b0006aed4f5,5d2018cfe2fb6b0006aed4ff',
  booking:
   '5d2018c7e2fb6b0006aed4f6,5d2018c7e2fb6b0006aed4f6,5d2018cfe2fb6b0006aed500,5d2018cfe2fb6b0006aed503,5d2018cfe2fb6b0006aed506,5d2018cfe2fb6b0006aed509,5d2018d0e2fb6b0006aed50c,5d2018cfe2fb6b0006aed500,5d2018cfe2fb6b0006aed503,5d2018cfe2fb6b0006aed506,5d2018cfe2fb6b0006aed509,5d2018d0e2fb6b0006aed50c',
  setup: '5d2018c9e2fb6b0006aed4fb',
  invoice: '5d2018d0e2fb6b0006aed511,5d2018d0e2fb6b0006aed511',
  metadata: '5d2018cae2fb6b0006aed4fc' }
[ { n: 2, ok: 1, deletedCount: 2 },
  { n: 1, ok: 1, deletedCount: 1 },
  { n: 1, ok: 1, deletedCount: 1 },
  { n: 2, ok: 1, deletedCount: 2 },
  { n: 6, ok: 1, deletedCount: 6 },
  { n: 1, ok: 1, deletedCount: 1 },
  { n: 1, ok: 1, deletedCount: 1 },
  { n: 0, ok: 1, deletedCount: 0 } ]
    ✓ DEL /systemadmin/test/data, Should remove testing data (181ms)


  110 passing (39s)

Done in 39.87s.

```

## [ Work Log ]
-------------------------------------
###### 30Oct2019
```
- Email updated for invite partner owner
- Email updated for cancel event
- Email updated for cancel booking
- Email updated for release event
- Email updated for event feedback
```
###### 27Oct2019
```
- Event populated partner, createdBy, approvedBy, cancelledBy
- Remove estimate status check
```
###### 25Oct2019
```
- Partner archived by infor
- Partner suspended by infor
- Simple credit system
- Events populated approvedBy, cancelledBy
- Partners populated archivedBy, suspendedBy
```
###### 18Oct2019
```
- Apply event discount
- Display event total price
```
###### 03Oct2019
```
- Apply invoice discount
- Cancel booing without cancel the event
- Reason for cancel
```
###### 29Sep2019
```
- New invoice builder
```
###### 22Sep2019
```
- Endpoint for estimate booking
```
###### 19Sep2019
```
- Delete cancelled events
- Admin can edit others booking after confirmed
- Admin can delete others booking after confirmed
```
###### 18Sep2019
```
- Show who approved an event
- Show who cancelled an event
```
###### 4Aug2019
```
- Parameter "bookingsTable" for event emails 
```
###### 29Jul2019
```
- Search bookings by "search", will search for both event name and partner name
- Search invoices by "search", will search for both event name and partner name
- Search events by "search", will search for both event name and partner name
- List partners should not return agreementDetail
- invNo field for invoice
```
###### 29Jul2019
```
- Search bookings by partnerName
- Search invoices by partnerName
- Search events by partnerName
- Removed Search Term partnerDisplayName, partnerBrname from events, bookings, invoices listing
```
###### 29Jul2019
```
- Search bookings by eventPublic, partnerDisplayName, partnerBrname
- Search invoices by partnerDisplayName, partnerBrname
- Removed event fields partnerDisplayName, partnerBrname, createdByAdmin
============================================================================
[ WARNING ] partnerDisplayName, partnerBrname, createdByAdmin fields have been REMOVED from event model. This version may break FE
Search events by partnerDisplayName, partnerBrname is still available
GET /events?sort=partnerDisplayName,partnerBrname 
should become 
GET /events?sort=partner.displayName,partner.brname
============================================================================
```
###### 25Jul2019
```
- Sort by multi fileds 
```
###### 24Jul2019
```
- Booking time set from 8am to 11pm HKT
```
###### 20Jul2019
```
- 7 days remind email should inculde invoice as attachment
```
###### 12Jul2019
```
- more optional fields for partner invite user
- Swagger update partner model
- Endpoint POST /user/:id/suspend
- Swagger endpoint POST /user/:id/suspend
- Graphql mutation user/suspend
- Update test case for suspend user
```
###### 10Jul2019
```
- no invoice on comfirm event
- updated invoice line detail
```
###### 09Jul2019
```
- no invoice on confirm, only when approve
- new email DIHK-EVENT-INVOICE-PAID
- search by event partner name
- event support [status]BYADMIN search. eg: DRAFTBYADMIN
```
###### 08Jul2019
```
- partner name case-insensitive sort
- ignore line item for invoice with 0 unit
- avoid invoice contain multi cancellation fee
- reject update event within 24 hours
- reject update setup within 24 hours
- release ONHOLD event should remove calendar event
- remove partner email from the email cc list
```
###### 07Jul2019
```
- event popluate createdBy phone
- booking available field
- show pdf in browser
- partner specialties search case-insensitive
```
###### 06Jul2019
```
- fix email record search
- turn on all testing and point to live
```
###### 05Jul2019
```
- confirm or approval event checking for booking time
- fix log level
- tidy up error response format
- testing code
```
###### 04Jul2019
```
- systemadmin endpoints
- get users in same partner permission
- return user and partner id in token
```
###### 02Jul2019
```
- improve api response time
- invoice email
- filter users by system access
- fix invite as systemadmin
- fix invoice permission
```
###### 01Jul2019
```
- Populated bookings
- Endpoint GET /invoice/:id/pdf
- Swagger GET /invoice/:id/pdf
- Cancel booking: Void invoice
- Cancel booking: Cancellation fee - within 24 hours
- Cancel booking: Cancellation fee - within 14 days
```
###### 30Jun2019
```
- Swagger GET /event/:id/check
- Endpoint GET /event/:id/check
- Graphql mutation/event/check
- Quickbooks Auth
- Quickbooks Auth
- Quickbooks Customer
- Quickbooks Invoice
- Quickbooks item
- Quickbooks Payment
- Minimum $3000 per holiday
```
###### 27Jun2019
```
- Reject booking outside HKT 9:30am to 9:30pm
- Reject booking less than 1 hour
- Create invoice when confirm event
- Swagger invoice model
- Swagger invoice endpoint GET /invoices
- Swagger invoice endpoint GET /invoice/:id
- Swagger invoice endpoint POST /invoice/:id/pay
- GraphQL mutation/invoice/pay
- GraphQL query invoice
- GraphQL query invoices
```
###### 25Jun2019
```
- google oauth always ask for consent
```
###### 24Jun2019
```
- Google calendar holiday list for booking price picker
- Same event, filter setups if no booking with the same room
- Cron job event-finished
- Cron job event-onhold-release
- Cron job event-remind-7
- Cron job event-remind-onhold-16
- reject on hold event within 14 days
- [fix] update setup layout error
```
###### 23Jun2019
```
- GraphQL mutation/setup/set
- [fix] Delete booking
- Sendgrid email integration
- Swagger email record endpoint GET /email-records
- Endpoint GET /email-records
- Login response header return swagger, graphiql, google links for SYSTEMADMIN
```
###### 22Jun2019
```
- Swagger all models description
- Booking get price logic update
- User only keep the 3 latest tokens at one time
- [fix] Metadata delete key error
- booking price unit round to 2dp
- booking price total round to the nearest dollar  
- turn on permission for all restful endpoint
- check event status before update/create booking and setup (only if event in draft)
- turn on graphql permission
```
###### 21Jun2019
```
- [Update] setup layout field
- Auto save event createdBy field
- GraphQL mutation/setup/updateLayout
- Swagger for setup endpoints PUT /setup/:id/layout
```
###### 20Jun2019
```
- GraphQL query events (Populated)
- Swagger for event endpoints GET /partner/:id/events (Populated)
- Swagger for event endpoints GET /events (Populated)
- Swagger for eventPopulated model
```
###### 17Jun2019
```
- Room availabilities
- Swagger for roomAvailabilities model
- Swagger for room endpoints GET /rooms/availabilities
- GraphQL query roomAvailabilities
```
###### 16Jun2019
```
- Room calendar id
- [update] Room showcases
- GraphQL mutation/room/setShowcase
- GraphQL mutation/room/deleteShowcase
- [update] Partner payment detail encrypted in database level
- [update] GraphQL query partners (No payment detail)
- [update] GraphQL mutation/partner/updateProfile (No payment detail)
- [update] GraphQL mutation/partner/invite (No payment detail)
- [update] GraphQL mutation/partner/cancelInvite (No payment detail)
```
###### 15Jun2019
```
- Room additionals
- Event on-hold
```
###### 14Jun2019
```
- invite user with system access
- [fix] invite register notification setting
```
###### 11Jun2019
```
- [fix] invite teammate as USER become OWNER
- [feature] GraphQL for mutation/metadata/setKey
- [feature] Swagger for metadata endpoints POST /metadatas/keys
- GraphQL query event 
- GraphQL query events
- GraphQL query booking 
- GraphQL query bookings
- GraphQL query setup 
- GraphQL query setups
- GraphQL for mutation/event/new
- GraphQL for mutation/event/delete
- GraphQL for mutation/event/update
- GraphQL for mutation/event/confirm
- GraphQL for mutation/event/cancel
- GraphQL for mutation/event/updateOfficialSetting
- GraphQL for mutation/booking/new
- GraphQL for mutation/booking/delete
- GraphQL for mutation/booking/update
- GraphQL for mutation/setup/new
- GraphQL for mutation/setup/delete
- GraphQL for mutation/setup/update
- Swagger for event model
- Swagger for booking model
- Swagger for setup model
- Swagger for 9 endpoints related to event
- Swagger for 6 endpoints related to booking
- Swagger for 6 endpoints related to setup
```
###### 07Jun2019
```
- GraphQL for mutation/room/delete
- [fix] authorization header
- GraphQL query metadata 
- GraphQL query metadatas
- GraphQL for mutation/metadata/new
- GraphQL for mutation/metadata/delete
- GraphQL for mutation/metadata/update
- Swagger for metadata model
- Swagger for metadata endpoints GET /metadatas
- Swagger for metadata endpoints POST /metadatas
- Swagger for metadata endpoints GET /metadata/:key
- Swagger for metadata endpoints PUT /metadata/:key
- Swagger for metadata endpoints DEL /metadata/:key
```
###### 06Jun2019
```
- Debug log for invite token
- [removed] GraphQL for mutation/partner/validateInvite
- [updated] Swagger for partner model (specialties)
- Swagger for auth endpoints GET /auth/invite-register/validate
- Swagger for auth endpoints POST /auth/invite-register
- [removed] Swagger for partner endpoints GET /partner/:id/invite/validate
- [removed] Swagger for partner endpoints POST /partner/:id/join
- Endpoints: GET /auth/invite-register/validate
- Endpoints: POST /auth/invite-register
- [removed] Endpoints: GET /partner/:id/invite/validate
- [removed] Endpoints: POST /partner/:id/join
```
###### 05Jun2019
```
- GraphQL for mutation/partner/new
- GraphQL for mutation/partner/invite
- GraphQL for mutation/partner/cancelInvite
- GraphQL for mutation/partner/validateInvite
- [updated] Swagger for partner model
- Swagger for partner endpoints POST /partner/:id/invite
- Swagger for partner endpoints POST /partner/:id/cancel-invite
- Swagger for partner endpoints GET /partner/:id/invite/validate
- Swagger for partner endpoints POST /partner/:id/join
- Endpoints: POST /partner/:id/invite
- Endpoints: POST /partner/:id/cancel-invite
- Endpoints: GET /partner/:id/invite/validate
- Endpoints: POST /partner/:id/join
```
###### 03Jun2019
```
- GraphQL for mutation/room/setPrice
- [updated] Swagger for room model
- Swagger for room endpoints POST /rooms/:id/prices
- Endpoints: POST /rooms/:id/prices
```
###### 02Jun2019
```
- [fixed] GraphQL query partner response schema
- [fixed] GraphQL mutation user response schema
- [fixed] GraphQL mutation partner response schema
- GraphQL for query/room
- GraphQL for query/rooms
- GraphQL for mutation/room/new
- GraphQL for mutation/room/update
- Swagger for room model
- Swagger for room endpoints
- Endpoints: POST /rooms
- Endpoints: GET /rooms
- Endpoints: GET /room/:id
- Endpoints: PUT /room/:id
```
###### 30May2019
```
- GraphQL for query/user
- GraphQL for query/users
- GraphQL for query/partner
- GraphQL for query/partners
- GraphQL for mutation/user/updateProfile
- GraphQL for mutation/user/updatePartnerSetting
- GraphQL for mutation/user/updateOfficialSetting
- GraphQL for mutation/user/updateSystemAccess
- GraphQL for mutation/partner/updateProfile
- GraphQL for mutation/partner/updatePayment
- GraphQL for mutation/partner/updateOfficialSetting
```
###### 29May2019
```
- Swagger for user model
- Swagger for partner model
- Swagger for token model
- Swagger for user endpoints
- Swagger for partner endpoints
- Endpoints: GET /partners
- Endpoints: GET /partner/:id
- Endpoints: PUT /partner/:id/profile
- Endpoints: PUT /partner/:id/payment
- Endpoints: PUT /partner/:id/official-setting
- Endpoints: GET /users
- Endpoints: GET /user/:id
- Endpoints: PUT /user/:id/profile
- Endpoints: PUT /user/:id/partner-setting
- Endpoints: PUT /user/:id/official-setting
- Endpoints: PUT /user/:id/system-access
```
###### 28May2019
```
- Swagger
  { api_url } /swagger
- Change password
  POST /auth/password
```
###### 27May2019
```
- authorization by token
  [ header ] Authorization : bearer JWT-TOKEN
  [ testing endpoint ] GET /user/mine
```
```
- Deployment Detail
  1) Build the image
     ./docker-build.sh
  2) copy run.env.sample
     cp run.env.sample run.env
  3) start docker image
     docker-compose up -d
```